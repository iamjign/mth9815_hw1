from __future__ import print_function, absolute_import, division
from periodic_payment_calculator import Periodic_payment_amt_calc
import collections, numpy as np, scipy as sp
from scipy.interpolate import interp1d


#Q2


##############################################

PRINCIPAL = 500000 # principal amount
RATE = 3.625/100./12. # make it a monthly interest rate
TERM = 360 # number of payments (months)
EXTRA_PAYMENT = 0 # extra amount of principal every


# The dict key is the month and the dict value is the percent rate.
# Month 12 is a year from now.
# Month 360 is 30 years from now
YC = { 1: 0.73818, 2: 0.83103, 3: 0.85393, 4: 0.87344,
       5: 0.89156, 6: 0.90619, 9: 0.94035, 12: 0.96712,
      18: 1.01837, 24: 1.05664212, 36: 1.11874651,
      48: 1.17941657, 60: 1.23843929, 72: 1.30179396,
      84: 1.36339275, 96: 1.42007995, 108: 1.47176883,
     120: 1.51815895, 144: 1.60114194, 180: 1.69000072,
     240: 1.78308961, 300: 1.82618131, 360: 1.84928439 }


def YC_linear_interpolation(given_incomplete_curve_dict):

    """This is a collection of steps to strip out yield curves and interpolate the yields on missing months"""

    #convert YC to ordered Dict using collections module's orderdictionary class

    od = collections.OrderedDict(sorted(given_incomplete_curve_dict.items()))

    #separate key and value for interpolation

    x1, y1 = zip(*od.iteritems())

    newlength = 360

    #even out intervals
    x2 = np.linspace(min(x1), max(x1), newlength)
    y2 = sp.interpolate.interp1d(x1, y1, kind='linear')(x2)
    YC_tuple = zip(x2, y2)
    YC_new = collections.OrderedDict()
    YC_new.update(YC_tuple)

    return YC_new


def NPV_semi_annual_calc(periodic_payment, yield_curve, term):

    """the yield curve discounts the fixed periodic payment each month,
    The formula used is sum(periodic payment/[(1+yield_curve_t/2)^2*t])
    aggregate efficient formula is used"""

    biannual_payment = periodic_payment * 6

    bc_NPV = 0.

    for i in xrange(1, int(term/6)+1):
        bc_NPV += biannual_payment / (1. + yield_curve[i * 6] / 200.) ** i


    return bc_NPV

if __name__ == '__main__':

    # a)
    YC_interpolated = YC_linear_interpolation(YC) #return an Orderdict object key = month, value = discount rate
    monthly_payment = Periodic_payment_amt_calc(PRINCIPAL, RATE, TERM, EXTRA_PAYMENT)
    print(monthly_payment)
    NPV = NPV_semi_annual_calc(monthly_payment, YC_interpolated, TERM)

    print("Q2: a) The present value of the current mortgage is {0}.".format(NPV))

    # b)
    """calculate the value of the mortgage if there is an extra payment done every year equal
    to the value of a single monthly payment. It will be easier if you split that extra yearly
    payment over every month.
    """
    EXTRA_PAYMENT_1 = 1.

    monthly_payment_prepayment = Periodic_payment_amt_calc(PRINCIPAL, RATE, TERM, EXTRA_PAYMENT_1)
    #print(monthly_payment_prepayment)
    NPV_prepayment = NPV_semi_annual_calc(monthly_payment_prepayment, YC_interpolated, 313)+982.051/(1+YC_interpolated[314]/2)**157

    print("Q2: b) The present value of the mortgage with a extra monthly \n       payment annually is {0}.".format(NPV_prepayment))
