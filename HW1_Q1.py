from __future__ import print_function, absolute_import, division
from periodic_payment_calculator import Periodic_payment_amt_calc

#Q1

##############################################

PRINCIPAL = 500000 # principal amount
RATE = 3.625/100./12. # make it a monthly interest rate
TERM = 360 # number of payments (months)
EXTRA_PAYMENT = 1 # extra amount of principal every month

def Amortizing_schedule(monthly_payment, principal, term):

    """printing out amortizing schedule"""

    #initialize
    balance = principal

    for i in xrange(term):

        interest_amount = balance*RATE

        principal_amount = monthly_payment-interest_amount

        balance -= principal_amount

        if balance >= monthly_payment:
            print("%i          %.3f    %.3f    %.3f   %.3f" % (i + 1, monthly_payment, principal_amount, interest_amount, balance))

        else:
            print("%i          %.3f    %.3f    %.3f   %.3f" % (i + 1, monthly_payment, principal_amount, interest_amount, balance))

            print("%i          %.3f     %.3f      %.3f    %.3f" % (i + 2, balance, balance, 0, 0))

            break










if __name__ == '__main__':

    monthly_payment = Periodic_payment_amt_calc(PRINCIPAL, RATE, TERM, EXTRA_PAYMENT)

    print(monthly_payment)
    print("Payment#    Amount    Principal   Interest    Balance")

    Amortizing_schedule(monthly_payment, PRINCIPAL, TERM)









