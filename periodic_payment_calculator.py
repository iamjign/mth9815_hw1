""""To better facilitate functions between questions, all shared functions will be declared
and exported to different questions"""
#define amortization function



def Periodic_payment_amt_calc(principal, rate, term, extra_payment):


    """calculating the payment amount per period"""

    N = (1+rate)**term

    monthly_payment = principal * rate * N / (N - 1)

    if extra_payment != 0:
        monthly_payment += monthly_payment*extra_payment/12.



    return monthly_payment

